param(
    $file=$args[0],
    $env=$args[1]
)

$env:FLASK_ENV = "$env"
$env:FLASK_APP = "$file"

flask run -h 0.0.0.0 -p 19090