show databases;
create database OS_database;

drop table history;

create table history(
	ds datetime not null comment '日期',
    confirm int default null comment '累计确诊',
    confirm_add int default null comment '新增确诊',
    suspect int default null comment '剩余疑似病例',
    suspect_add int default null comment '新增疑似病例',
    heal int default null comment '累计治愈',
    heal_add int default null comment '当日新增治愈',
    dead int default null comment '累计死亡',
    dead_add int default null comment '当日新增死亡',
    primary key (ds) using btree
)engine=InnoDB default charset=utf8mb4;

drop table details;

create table details(
	id int not null auto_increment,
    update_time datetime default null comment '更新时间',
    province varchar(11) default null comment '省份',
    city varchar(11) default null comment '城市',
    confirm int default null comment '累计确诊',
    confirm_add int default null comment '新增确诊',
    heal int default null comment '治愈',
    dead int default null comment '死亡',
    primary key(id)
)engine=InnoDB default charset=utf8mb4;

drop table hotsearch;

create table hotsearch (
	id int not null auto_increment,
    dt datetime default null on update current_timestamp,
    content varchar(255) default null,
    hot_value int default null,
    primary key(id)
)engine=InnoDB default charset=utf8mb4;

# 获取全国疫情相关数字总数
select sum(confirm) s_confirm,
(select suspect from history order by ds desc limit 1) s_suspect,
sum(heal) s_heal, sum(dead) s_dead 
from details 
where update_time=(select update_time from details order by update_time desc limit 1)

# 获取疫情地图相关数据（省）
select province, sum(confirm) from details 
where update_time=(select update_time from details 
order by update_time desc limit 1) 
group by province

# 获取省级疫情相关数字
select province, sum(confirm), sum(heal), sum(dead) from details   
where update_time=(select update_time from details 
order by update_time desc limit 1) 
group by province

# 全国累计/新增确诊的时间走向图
SELECT ds, confirm, confirm_add FROM history;

# 获取热搜词
select content,hot_value from hotsearch order by id desc limit 20
