from selenium.webdriver import Chrome,ChromeOptions
# 这里注意一下 selenium 4.0.0版本后 需要额外导入一个包
from selenium.webdriver.common.by import By
import time

def get_hot_search()->list:
    ts = time.strftime("%Y-%m-%d %X");
    option = ChromeOptions()
    option.binary_location = './chrome_binary/chrome.exe'
    chrome_driver_binary = './chromedriver.exe'
    option.add_argument("--headless") #开启无头模式
    option.add_argument("--no-sandbox") #关闭沙盒模式
    data = []
    # 这里的参数是浏览器驱动的路径，若在驱动在当前文件目录下，可以忽略不写
    browser = Chrome(chrome_driver_binary, options=option)
    url = 'https://top.baidu.com/board?tab=realtime'
    browser.get(url)
    # 这里注意一下 selenium 4.0.0版本后 需要使用find_elements/find_element
    # elements = browser.find_elements_by_css_selector( '.c-single-text-ellipsis')
    elements = browser.find_elements( By.XPATH, '//*[@id="sanRoot"]/main/div[2]/div/div[2]/div/div[2]/a/div[1]')
    # 这里只收录了热词，没有收录热力值，优化：收录热力值作为词云的value
    elements_value = browser.find_elements( By.XPATH, '//*[@id="sanRoot"]/main/div[2]/div/div[2]/div/div[1]/div[2]')
    for i in range(len(elements)):
        ele = elements[i]
        ele_v = elements_value[i]
        data.append([ele.text, ele_v.text, ts])
    browser.close()
    return data

if __name__ == '__main__':
    pass