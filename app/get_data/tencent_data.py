import requests
import json
import time
"""
return: 返回历史数据和当日详细数据
"""
def get_tencent_data()->dict:
    curday_info_url='https://view.inews.qq.com/g2/getOnsInfo?name=disease_h5'
    history_info_url='https://view.inews.qq.com/g2/getOnsInfo?name=disease_other'
    curday_info=__getdata_by_url(curday_info_url)
    history_info=__getdata_by_url(history_info_url)
    history={}
    for i in history_info['chinaDayList']:
        ds='%s.%s'%(i['y'], i['date']) #2021.09.06
        ds=time.strftime('%Y-%m-%d',time.strptime(ds,'%Y.%m.%d')) # 转换时间格式以符合数据库datatime类型
        confirm=i['confirm'] #累计感染
        suspect=i['suspect'] #疑似
        dead=i['dead'] #死亡
        heal=i['heal'] #治愈
        nowConfirm=i['nowConfirm'] #当前感染
        nowSevere=i['nowSevere'] #当前重症
        importedCase=i['importedCase'] #输入性病例
        deadRate=i['deadRate'] #死亡率
        healRate=i['healRate'] #治愈率
        history[ds]={'confirm': confirm,'suspect':suspect,'dead':dead,'heal':heal}

    for i in history_info['chinaDayAddList']:
        ds='%s.%s'%(i['y'], i['date']) #2021.09.06
        ds=time.strftime('%Y-%m-%d',time.strptime(ds,'%Y.%m.%d')) # 转换时间格式以符合数据库datatime类型
        confirm=i['confirm'] #累计感染
        suspect=i['suspect'] #疑似
        dead=i['dead'] #死亡
        heal=i['heal'] #治愈
        history[ds].update({'confirm_add': confirm,'suspect_add':suspect,'dead_add':dead,'heal_add':heal})
    details=[]
    update_time=curday_info['lastUpdateTime']
    data_country=curday_info['areaTree']
    data_province=data_country[0]['children']
    for pro_infos in data_province:
        province=pro_infos['name'] #省名
        for city_infos in pro_infos['children']:
            city=city_infos['name']
            confirm=city_infos['total']['confirm']
            confirm_add=city_infos['today']['confirm']
            heal=city_infos['total']['heal']
            dead=city_infos['total']['dead']
            details.append([update_time, province, city, confirm, confirm_add, heal, dead])
    return history,details

def __getdata_by_url(url:str)->dict:
    res=requests.get(url)
    d=json.loads(res.content.decode('utf8'))
    return json.loads(d['data'])
    
if __name__ == '__main__':
    # bilibil()
    pass