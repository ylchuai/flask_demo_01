import pymysql
# pip i configparser
from configparser import ConfigParser

def _getConfig()->ConfigParser:
    cf = ConfigParser()
    cf.read("./config.ini")
    # base API of ConfigParser
    # print(cf.sections())
    # print(cf.options("Mysql-Database"))
    # print(cf.items("Mysql-Database"))
    # print(cf.get("Mysql-Database", "host"))
    return cf

def get_conn():
    cf = _getConfig()
    # 这里的host user password db请适配自己的数据库
    conn=pymysql.connect(host=cf.get("Mysql-Database", "host"),
                        user=cf.get("Mysql-Database", "user"),
                        password=cf.get("Mysql-Database", "password"),
                        db=cf.get("Mysql-Database", "db"))
    cursor=conn.cursor()
    return conn,cursor

def closeConn(conn, cursor):
    if conn:
        conn.close()
    if cursor:
        cursor.close()
        
if __name__ == "__main__":
    # (conn, cursor) = get_conn()
    # print(closeConn(conn, cursor))
    # _getConfig()
    print(_getConfig().get("Mysql-Database", "host"))
    pass