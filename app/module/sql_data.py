import time

from utils import sql_util
from get_data import tencent_data,baidu_data

def saveHistory(data: dict):
    conn, cursor = sql_util.get_conn()
    sql_insert = 'insert into history values(%s,%s,%s,%s,%s,%s,%s,%s,%s)'
    # data => item => list => 逆序 从反方向上获取数据，遇到重复的数据就停止循环
    # 缺点：如果之前的记录是非连续的 就会导致非连续的部分无法复原
    for k, v in reversed(list(data.items())):
        # 如果查询不到，新增数据
        if not get_history(k):
            cursor.execute(sql_insert, [
                k, v['confirm'], v['confirm_add'], v['suspect'], v['suspect_add'], v['heal'], v['heal_add'], v['dead'], v['dead_add']
            ])
    conn.commit()
    sql_util.closeConn(conn, cursor)

def _query(sql):
    conn, cursor = sql_util.get_conn()
    cursor.execute(sql)
    sql_util.closeConn(conn, cursor)
    return cursor.fetchall()

def get_history(ds):
    conn, cursor = sql_util.get_conn()
    sql_get_by_ds = 'select ds from history where ds=%s'
    has_data = cursor.execute(sql_get_by_ds, ds)  # 返回int 1有数据，0无数据
    sql_util.closeConn(conn, cursor)
    return has_data


def update_details(data):
    conn, cursor = sql_util.get_conn()
    sql_insert = 'insert into details(update_time, province, city, confirm, confirm_add, heal, dead) values(%s,%s,%s,%s,%s,%s,%s)'
    if not getDetails(data[0][0]):
        for i in data:
            cursor.execute(sql_insert, i)
    conn.commit()
    sql_util.closeConn(conn, cursor)


def getDetails(updateTime):
    conn, cursor = sql_util.get_conn()
    # 对比当前最大时间戳
    sql_get_details_by_update_time = 'select %s=(select update_time from details order by id desc limit 1)'
    cursor.execute(sql_get_details_by_update_time, updateTime)
    sql_util.closeConn(conn, cursor)
    return cursor.fetchone()[0]

def update_hot_search(): #data = get_hot_search()
    data = baidu_data.get_hot_search()
    conn,cursor=sql_util.get_conn()
    sql_insert='insert into hotsearch(content, hot_value, dt) values(%s,%s,%s)'
    for i in data:
        # print(i[0], i[1])
        cursor.execute(sql_insert, i)
    conn.commit()
    sql_util.closeConn(conn,cursor)
    
def updateDatebase():
    (history_data, details_data) = tencent_data.get_tencent_data()
    # 更新数据
    saveHistory(history_data)
    update_details(details_data)
    update_hot_search()


def get_cl_data():
    # 读取数据
    conn, cursor = sql_util.get_conn()
    sql_get_curday_details = '''
    select sum(confirm) s_confirm,
    (select suspect from history order by ds desc limit 1) s_suspect,
    sum(heal) s_heal, sum(dead) s_dead 
    from details 
    where update_time=(select update_time from details order by update_time desc limit 1)
    '''
    cursor.execute(sql_get_curday_details)
    sql_util.closeConn(conn, cursor)
    return cursor.fetchone()


def get_c2_data():
    sql = '''
	select province, sum(confirm) from details 
    where update_time=(select update_time from details 
    order by update_time desc limit 1) 
    group by province
    '''
    return _query(sql)

def get_c3_data():
    sql = '''
	select province, sum(confirm), sum(heal), sum(dead) from details   
    where update_time=(select update_time from details 
    order by update_time desc limit 1) 
    group by province
    '''
    return _query(sql)

def get_history_confirm():
    sql = '''
    SELECT ds, confirm, confirm_add FROM history;
    '''
    return _query(sql)

def get_details_by_province(province):
    conn, cursor = sql_util.get_conn()
    sql = '''
    SELECT city, confirm, confirm_add, heal, dead 
    FROM details 
    WHERE province=%s 
    AND update_time=
        (SELECT update_time 
        FROM details 
        ORDER BY id 
        DESC LIMIT 1
        ); 
    '''
    cursor.execute(sql, province) 
    sql_util.closeConn(conn, cursor)
    return cursor.fetchall()

def get_hot_search():
    ds = time.strftime("%Y{}%m{}%d %X").format('年','月','日')
    sql = '''
    select content,hot_value from hotsearch order by id desc limit 20
    '''
    return _query(sql)


if __name__ == '__main__':
    pass