import es_center from './es_center.js'
import es_panel from './es_panel.js'
import es_wordcloud from './es_wordcloud.js'

$(document).ready(() => {
    renderLocalTime();
    renderNum();
    get_map_data();
    get_history_confirm();
    init_province();
    get_hot_search();
    c1Draw()
    c2Draw()
    // echarts 
    // E_INIT.init4()

});

// 基于python接口绘制时间
function renderTime() {
    // 获取时间
    $.ajax({
        // 请求地址
        url: "/time",
        // 请求类型
        type: "GET",
        // 数据
        data: {},
        success: (data) => {
            $("#time").text(data);
            setTimeout(renderTime, 1000);
        },
        error: (xhr, type, errorThrown) => { },
    });
}

function renderNum() {
    // 获取腾讯接口的统计信息
    $.ajax({
        // 请求地址
        url: "/get_tencent_sum_data",
        // 请求类型
        type: "GET",
        // 数据
        data: {},
        success: (data) => {
            // console.log(data)
            $(".no-show div").eq(0).text(data.confirm);
            $(".no-show div").eq(1).text(data.suspect);
            $(".no-show div").eq(2).text(data.heal);
            $(".no-show div").eq(3).text(data.dead);
            let country = [data.confirm, data.heal, data.dead];
            localStorage.setItem('country_data', JSON.stringify(country))
            es_panel.init_province(1, '全国', country)
            // setTimeout(renderNum, 60000);
        },
        error: (xhr, type, errorThrown) => { },
    });
}

function get_map_data() {
    $.ajax({
        url: '/get_map_data',
        type: 'GET',
        data: {},
        success: data => {
            localStorage.setItem('全国_data', JSON.stringify(data))
            es_center.renderMap(data);
        },
        error: (xhr, type, errorThrown) => { es_center.renderMap([]); },
    });
}

function get_hot_search() {
    $.ajax({
        url: '/get_hot_search_data',
        type: 'GET',
        data: {},
        success: data => {
            es_wordcloud.init(data);
        },
        error: (xhr, type, errorThrown) => { },
    });
}
/**
 * [
  {
    "confirm": 123361, 
    "confirm_add": 39, 
    "ds": "2021-09-08"
  }, 
  {
    "confirm": 123386, 
    "confirm_add": 25, 
    "ds": "2021-09-09"
  }, 
  {
    "confirm": 123423, 
    "confirm_add": 37, 
    "ds": "2021-09-10"
  }, 
]
 */
function get_history_confirm() {
    $.ajax({
        url: '/get_history_confirm',
        type: 'GET',
        data: {},
        success: datas => {
            let datax = [], datay = { confirm: [], confirm_add: [] }
            for (let data of datas) {
                datax.push(data.ds);
                datay.confirm.push(data.confirm / 1000);
                datay.confirm_add.push(data.confirm_add);
            }
            es_panel.init_confirm(0, datax, datay)
        },
        error: (xhr, type, errorThrown) => { es_panel.init_confirm(0, [], { confirm: [], confirm_add: [] }); },
    });

}

function init_province() {
    $.ajax({
        url: '/get_province_data',
        type: 'GET',
        data: {},
        success: datas => {
            localStorage.setItem('province_data', JSON.stringify(datas))
        },
        error: (xhr, type, errorThrown) => { },
    });
}
/*-----------------------------canvas start-------------------- */
function polygon(c, n, x, y, r, angle, counterclockwise) {
    var res = [];
    var angle = 0;
    var counterclockwise = false;
    res.push([x + r * Math.sin(angle), y - r * Math.cos(angle)]);
    c.moveTo(x + r * Math.sin(angle), y - r * Math.cos(angle)); //确立第一个点
    var delta = (2 * Math.PI) / n; //相邻两个顶点之间的夹角
    for (var i = 0; i < n; i++) {
        //其他顶点
        if (i % 2 == 1) {
            res.push([x + r * Math.sin(angle), y - r * Math.cos(angle)]);
        }
        angle += counterclockwise ? -delta : delta; //角度调整
        c.lineTo(x + r * Math.sin(angle), y - r * Math.cos(angle));
    }
    c.closePath(); //首位相邻
    return res;
}

function c1Draw() {
    let c1 = document.querySelector("#c1");
    c1 = c1.getContext("2d");
    let points = polygon(c1, 12, 500, 500, 450);
    c1.lineWidth = 10;
    c1.strokeStyle = "#4d4af825";
    c1.stroke();
    for (let point of points) {
        c1.beginPath();
        c1.arc(point[0], point[1], 25, 0, 2 * Math.PI);
        c1.fillStyle = "#4d4af825";
        c1.fill()
    };
}

function c2Draw() {
    let c2 = document.querySelector("#c2"), x, y;
    c2 = c2.getContext("2d");
    x = 500, y = 30;
    c2.moveTo(x, y);
    c2.lineTo(500 + 40 * Math.cos(Math.PI / 6), 35 + 40 * Math.sin(Math.PI / 6));
    c2.lineTo(510, 35)
    c2.lineTo(500 + 40 * Math.cos(Math.PI / 6), 35 - 40 * Math.sin(Math.PI / 6));
    c2.lineTo(500, 35)

    c2.closePath(); //首位相邻
    //定位起点
    // c2.arc(500, 35,10,0,2*Math.PI);
    c2.fillStyle = "#0fccfc";
    c2.fill()
    // 创建渐变色
    var grd = c2.createLinearGradient(500, 0, 590, 0);
    grd.addColorStop(0, "#0fccfc");
    grd.addColorStop(1, "white");

    c2.beginPath();
    c2.moveTo(500, 35)
    c2.arc(500, 500, 465, -Math.PI * 119 / 240, -Math.PI * 115 / 240);
    c2.lineWidth = 5;
    c2.strokeStyle = grd;
    c2.stroke();

}
/*-----------------------------canvas end---------------------- */



function renderLocalTime() {
    // TODO
    let d = new Date(),
        // number format 方法 转字符串并补0
        nf = (n) => n.toString().padStart(2, "0");
    $("#time").text(`当前时间：${d.getFullYear()}年
    ${nf(d.getMonth() + 1)}月
    ${nf(d.getDate())}日 
    ${nf(d.getHours())}时
    ${nf(d.getMinutes())}分
    ${nf(d.getSeconds())}秒`);
    setTimeout(renderLocalTime, 1000);
}