
/**
 * [方法去抖]
 *
 * @param   {[Function]}  fn     [需要去抖的方法]
 * @param   {[Number]}  delay  [去抖延时]]
 *
 * @return  {[Function]}         [已去抖的方法]]
 */
const debounde = function (fn, delay) {
    let timer = null;
    return (...arg) => {
        if (timer) clearTimeout(timer);
        timer = setTimeout(fn, delay, ...arg);
    }
}

export default {
    debounde
}