function getEchartsBox(index) {
  let position = index > 2 ? 'cr' : 'cl';
  return echarts.init(document.querySelector('.' + position + ' .panel:nth-child(' + (index % 3 + 1) + ') .chart'))
}
function getTitleBox(index) {
  let position = index > 2 ? 'cr' : 'cl';
  return document.querySelector('.' + position + ' .panel:nth-child(' + (index % 3 + 1) + ') h2')
}
/**
 * 疫情感染人数趋势图
 *
 * @param   {Number}  index  echarts图表的定位 左上角0 左下角2 右上角3 右下角5
 * @param   {Arrat}  datax   日期列表
 * @param   {Object}  datay  地区疫情数据
 *          {confirm, confirm_add} 感染 新增感染
 *  
 */
function init_confirm(index, datax, datay) {
  let max_x = Math.max(...datay.confirm);
  let min_x = Math.min(...datay.confirm);
  let max_y = Math.max(...datay.confirm_add);
  let min_y = Math.min(...datay.confirm_add);
  let option = {
    // title: {
    //   text: '全国累计与增长趋势',
    //   textStyle: {
    //     color: '#fff'
    //   },
    //   left: 'left'
    // },
    xAxis: {
      data: datax,
      axisLine: {
        lineStyle: {
          color: '#ffd700'
        }
      }
    },
    yAxis: [{
      name: '人数（千人）', min: Math.ceil(min_x*0.9), max: Math.ceil(max_x*1.1),
      axisLine: {
        lineStyle: {
          color: '#ffd700'
        }
      }
    }, {
      name: '人数', min: 0, max: Math.ceil(max_y*1.1),
      axisLine: {
        lineStyle: {
          color: '#ffd700'
        }
      }
    }],
    tooltip: {
      trigger: 'axis'
    },
    grid: {
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true
    },
    series: [
      {
        data: datay.confirm,
        name: '累计确诊',
        type: 'line',
        color: '#5087ff'
      },
      {
        data: datay.confirm_add,
        name: '新增确诊',
        yAxisIndex: 1,
        type: 'line',
        smooth: true,
        color: '#cf5252'
      }
    ]
  };
  let title = getTitleBox(index);
  title.innerText = '全国累计与增长趋势';
  let echarts = getEchartsBox(index);
  setTimeout(function () {
    window.addEventListener('resize', function () {
      echarts.resize();
    });
  }, 200)
  echarts.setOption(option)
}
/**
 * 各省疫情人数统计柱状图
 *
 * @param   {Number}  index  echarts图表的定位 左上角0 左下角2 右上角3 右下角5
 * @param   {String}  area   地区名称
 * @param   {Array}  datas  地区疫情数据
 *          [confirm, heal, dead] 感染 治愈 死亡
 *  
 */
function init_province(index, area, datas) {
  let option = {
    // title: {
    //   text: area + '详情',
    //   textStyle: {
    //     color: '#fff'
    //   },
    //   left: 'left'
    // },
    xAxis: {
      data: ['累计确诊', '累计治愈', '累计死亡'],
      name: '分类',
      axisLine: {
        lineStyle: {
          color: '#ffd700'
        }
      }
    },
    yAxis: {
      name: '人数',
      axisLine: {
        lineStyle: {
          color: '#ffd700'
        }
      }
    },
    tooltip: {
      trigger: 'axis'
    },
    grid: {
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true
    },
    series: [
      {
        data: datas,
        type: 'bar',
        color: '#5087ff'
      }
    ]
  };
  let title = getTitleBox(index);
  title.innerText = area + '详情';
  let echarts = getEchartsBox(index);
  setTimeout(function () {
    window.addEventListener('resize', function () {
      echarts.resize();
    });
  }, 200)
  echarts.setOption(option)
}
export default {
  init_confirm,
  init_province
}