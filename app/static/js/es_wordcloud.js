    let index = 2, position = index > 2 ? 'cr' : 'cl';
    let ec_world_cloud = echarts.init(document.querySelector('.' + position + ' .panel:nth-child(' + (index % 3 + 1) + ') .chart'));
    export default {
        init(data) {
            let ec_world_cloud_options = {
                title: {
                    text: '今日热搜',
                    textStyle: {
                        color: 'white'
                    },
                    left: 'left'
                },
                tooltip: {
                    show: false
                },
                series: [{
                    type: 'wordCloud',
                    gridSize: 8,
                    sizeRange: [8, 25],
                    shape: 'circle',
                    rotationRange: [-45, 0, 45, 90],
                    rotationStep: 45,
                    textStyle: {
                        color: function () {
                            let color = 'rgb(' + Math.random() * 255 + ', ' + Math.random() * 255 + ', ' + Math.random() * 255 + ')';
                            return color;
                        }
                    },
                    left: 'center',
                    top: 'center',
                    width: '70%',
                    height: '80%',
                    right: null,
                    bottom: null,
                    layoutAnimation: true,
                    data: data
                }]
            };
            ec_world_cloud.setOption(ec_world_cloud_options);
        }
    }