import Util from './util.js'
import es_panel from './es_panel.js'
import pinyin from './pinyin.js'
let ec_center = echarts.init(document.querySelector('.map .chart'));

// 为 方法[render_province] 增加去抖功能
/**什么是去抖？
 * 事件频繁被触发，因而频繁执行DOM操作、资源加载等重行为
 */
const render_province_debounde = Util.debounde(render_province, 300),
    init_province_debounde = Util.debounde(es_panel.init_province, 300);
/**
 * 地图信息联动省信息柱状图
 *
 * @param   {String}  province  地区名称
 *
 */
function render_province(province) {

    let datas = localStorage.getItem('province_data');
    // console.log(data)
    datas = JSON.parse(datas)
    let data = datas.filter(bean => {
        return bean.province == province
    })[0];
    if (!data) {
        // bugfix 南海群岛会触发事件。但南海群岛无对应数据
        // console.log(province) // undefined
        return;
    }
    // console.log(data)//{confirm: '1153', dead: '1', heal: '622', province: '内蒙古'}
    es_panel.init_province(1, province, [data.confirm, data.heal, data.dead]);
}

function loadmap(name, cb) {
    let script = document.createElement('script');
    script.src = `./static/js/map/${name}.js`;
    script.addEventListener('load', () => {
        cb()
    })
    document.body.appendChild(script);
}

export default {
    renderMap(data) {//[{ 'name': '上海', 'value': 318 }, { 'name': '云南', 'value': 162 }]
        let ec_center_option = {
            title: {
                text: '',
                subtext: '',
                x: 'left'
            },
            tooltip: {
                trigger: 'item',
                backgroundColor: 'rgba(50,50,50,0.5)',
                borderColor: 'rgba(50,50,50,0)',
                extraCssText: 'width: 90px;height: 40px;padding: 0 0 0 8px;',
                formatter: '地区&emsp;{b}<br>确诊&emsp;{c}',
                textStyle: {
                    color: '#fff',
                    fontSize: 14,
                }
            },
            visualMap: {
                show: true,
                x: 'left',
                y: 'bottom',
                textStyle: {
                    fontSize: 15,
                    color: '#fff'
                },
                splitList: [{ start: 1, end: 9 },
                { start: 10, end: 99 },
                { start: 100, end: 999 },
                { start: 1000, end: 9999 },
                { start: 10000 }],
                color: ['#8A331099', '#C6491899', '#F5582599', '#F2AD9299', '#F9DCD199']
            },
            series: [{
                name: '累计确诊人数',
                type: 'map',
                mapType: 'china',
                roam: false,
                // silent: true,
                itemStyle: {
                    normal: {
                        borderWidth: 2,// 区域边框宽度
                        borderColor: '#009fe8',
                        areaColor: '#0d408388'
                    },
                    emphasis: {
                        borderWidth: 2,// 区域边框宽度
                        borderColor: '#009fe8',
                        areaColor: '#0d408388'
                    }
                },
                selectedMode: false,
                label: {
                    normal: {
                        show: true,
                        fontSize: 15,
                        color: '#fff'
                    },
                    emphasis: {
                        show: true,
                        fontSize: 15
                    }
                },
                data
            }]
        },
            timer = null,
            // 区域选中锁，有数据时，不再触发mouse相关事件
            area_show = '全国',
            area_lock = false;
        ec_center.setOption(ec_center_option)
        /**
         * 定义点击事件，进入/退出省级、直辖市地图
         * 固化1号窗口数据显示
         *
         */
        ec_center.on('click', function (e) {
            console.log(e)
            area_lock = e.name;
            if (e.name == "南海诸岛") return;
            console.log(area_lock)
            // e.preventDefault();
            // bugfix 南海群岛会触发事件。但南海群岛无对应数据
            render_province(e.data && e.data.name);
            // if (area_lock == e.name || e.name == "南海诸岛") {
            //     area_lock = false;
            //     return false
            // } else
            //     area_lock = e.name;
            // e.stopPropagation && e.stopPropagation();
            ec_center.dispatchAction({
                type: 'downplay',
                dataIndex: e.dataIndex,
            })
            load_province_detail(e.name, province_data => {
                console.log(province_data)
                pinyin[e.name] && loadmap(pinyin[e.name], () => {
                    area_show = area_lock;
                    ec_center_option.series[0].data = province_data;
                    ec_center_option.series[0].mapType = e.name;
                    ec_center.setOption(ec_center_option);
                })
            });
            setTimeout(() => {
                area_lock = false;
            }, 0);
            render_province_debounde(e.data && e.data.name);
        })
        document.querySelector('#main section div.map div.chart')
            .addEventListener('click', () => {
                if (area_lock) return;
                loadmap('china', () => {
                    ec_center_option.series[0].data = JSON.parse(localStorage.getItem('全国_data'));
                    ec_center_option.series[0].mapType = 'china'
                    ec_center.setOption(ec_center_option);
                    area_show = '全国';
                    init_province_debounde(
                        1,
                        area_show,
                        JSON.parse(localStorage.getItem('country_data'))
                    );
                })
            });

        /**
         * 鼠标移动相关事件，联动1号窗口数据显示
         */
        ec_center.on('mouseover', function (e) {
            // if (typeof area_lock == 'string') return;
            clearTimeout(timer)
            // bugfix 南海群岛会触发事件。但南海群岛无对应数据
            // render_province(e.data && e.data.name);
            render_province_debounde(e.data && e.data.name);
        })
        ec_center.on('mouseout', function (e) {
            // if (typeof area_lock == 'string') return;
            /*鼠标离开边界后开启定时器，准备还原为全国数据 */
            timer = setTimeout(() => {
                if(area_show == '全国')
                    init_province_debounde(1, area_show, JSON.parse(localStorage.getItem('country_data')))
                else
                    render_province_debounde(area_show);
            }, 100)
        })

        function load_province_detail(province, cb) {
            let province_data = localStorage.getItem(province + '_data');
            if (province_data) {
                cb(JSON.parse(province_data))
                return;
            }
            $.ajax({
                url: '/get_province_detail?province='+province,
                type: 'GET',
                data: {},
                success: datas => {
                    datas.forEach(bean => {
                        bean.name = bean.area+'市';
                        bean.value = bean.confirm;
                    })
                    localStorage.setItem(province+'_data', JSON.stringify(datas))
                    cb(datas)
                },
                error: (xhr, type, errorThrown) => { },
            });
        }

        setTimeout(function () {
            window.addEventListener('resize', function () {
                ec_center.resize();
            });
        }, 200)
    }
}
