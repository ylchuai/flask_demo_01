from flask import Flask, request
from flask import render_template
from flask import jsonify
import jieba.analyse

from module import sql_data

flask_app = Flask(__name__)

@flask_app.route('/')
def home_page():
    return render_template('/homepage/index.html')

@flask_app.route("/get_tencent_sum_data")
def get_tencent_sum_data():
    (confirm,suspect,heal,dead)=sql_data.get_cl_data()
    return jsonify({'confirm':confirm,'suspect':suspect,'heal':heal,'dead':dead})

@flask_app.route("/get_map_data")
def get_map_data():
    datas = sql_data.get_c2_data()
    res = []
    for data in datas:
        (name, value)=data
        res.append({'name': name, 'value': str(value)})
    return jsonify(res)

@flask_app.route("/get_history_confirm")
def get_history_confirm():
    datas = sql_data.get_history_confirm()
    res = []
    for data in datas:
        (ds, confirm, confirm_add)=data
        ds = ds.strftime('%Y-%m-%d')
        res.append({'ds': ds, 'confirm': confirm, 'confirm_add': confirm_add})
    return jsonify(res)
# 省统计数据
@flask_app.route("/get_province_data")
def get_province_data():
    datas = sql_data.get_c3_data()
    res = []
    for data in datas:
        (province, confirm, heal, dead)=data
        res.append({'province': province, 'confirm': confirm, 'heal': heal, 'dead': dead})
    return jsonify(res)

# 省具体数据
@flask_app.route("/get_province_detail")
def get_details_by_province():
    province=request.args['province']
    datas=sql_data.get_details_by_province(province)
    res=[]
    for data in datas:
        (area, confirm, confirm_add, heal, dead)=data
        res.append({'area': area, 'confirm': confirm, 'confirm_add': confirm_add, 'heal': heal, 'dead': dead})
    return jsonify(res)

@flask_app.route("/get_hot_search_data")
def get_hot_search_data():
    datas = sql_data.get_hot_search()
    res = []
    for data in datas:
        (content,hot_value)=data
        ks = jieba.analyse.extract_tags(content);
        for k in ks:
            res.append({'name': k, 'value': hot_value})
    return jsonify(res)

@flask_app.route("/update_database")
def update_database():
    sql_data.updateDatebase()
    return jsonify({'msg': 'success'})
        
# if __name__ == '__main__':
#     flask_app.run(debug=True)